# How to install Jmeter

## Install HomeBrew
First, we are going to install HomeBrew:
To install HomeBrew:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

let’s now update HomeBrew package definitions:

`brew update`
#### Make sure to update brew before installing JMeter, otherwise you may run into issues like this one:

```
==> Downloading http://www.apache.org/dyn/closer.cgi?path=jmeter/binaries/ap‌​ache-jmeter-2.11.tgz ==> Best Mirror http://apache.mirrors.hoobly.com/jmeter/binaries/apache-jmet‌​er-2.11.tgz curl: (22) The requested URL returned error: 404 Not Found Error: Failed to download resource "jmeter" Download failed: http://apache.mirrors.hoobly.com/jmeter/binaries/apache-jmet‌​er-2.11.tgz
```
## Install JMeter
It’s almost finished now, run:

`brew install jmeter`

If you wish to install jmeter-plugins along with JMeter, add the --with-plugins flag. After a couple of minutes, JMeter should be installed and ready to use.

`brew install jmeter --with-plugins`

### Launch JMeter
JMeter should be now available as a command-line, try jmeter `-h` and you should see something like:


![Pandao editor.md](Jmeter/photo/photo_1.png`)

To Launch JMeter, simply run `jmeter`:

`bobchen@Bob-Chens-MacBook-Pro-2% jmeter`

[Download Apache JMeter](https://jmeter.apache.org/download_jmeter.cgi)
[Jmeter Plugin](https://jmeter-plugins.org/)


[JDK Version update](https://mkyong.com/java/how-to-install-java-on-mac-osx/#homebrew-install-latest-java-on-macos)


# How to Create Basic Test and Report

## Add Thread Group

1. click Right mouse button on side bar 'Test Plan'
2. Add > Threads(Users) > Thread Group

### Edit Thread Group Setting

1. Number of Threads (users)：執行緒數量，相當於使用者數量
2. Ramp-up period (seconds)：該時間內啟動完所有的執行緒
3. Loop Count：上述兩樣的設定值，執行的重複次數

## Add Http Request

1. click Right mouse button on side bar 'Thread Group' 
2. Add > Sampler > HTTP Request

### Edit Http Request

1. Protocol [http]：通訊協定 以 http 與 https 
2. Server Name or IP：主機名稱或 IP 位置
3. HTTP Method：可以透過下拉式選單來選擇所用的 method
4. Path：路徑

     sample
        Protocol[http] : "https"
       Server Name or IP: "www.google.com"
        HTTP Method: GET
        Path: "/search?         q=shopline&ei=XoUfYYG8ObK1mAWXjavQCw&oq=shopline&gs_lcp=Cgdnd3Mtd2l6EAMyDgguEIAEEMcBEK8BEJMCMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEOgcIABBHELADOgcIABCwAxBDOgQIABBDOgsILhCABBDHARCjAjoFCAAQkQI6CwguEIAEEMcBENEDOgUILhCABDoLCC4QgAQQxwEQrwE6DQguEMcBEKMCEEMQkwI6CgguEMcBENEDEENKBAhBGABQ0I87WMaVO2DOlztoA3ACeACAAa4BiAH8B5IBAzIuN5gBAKABAcgBCsABAQ&sclient=gws-wiz&ved=0ahUKEwiB48_Gs7_yAhWyGqYKHZfGCroQ4dUDCA4&uact=5"

      Path use search "Shopline"?>

### Add Results Tree Report

1. Click Right mouse button on side bar 'Thread Group'
2. Add > Listener > View Results Tree

### Add Aggregate Report Report

1. Click Right mouse button on side bar 'Thread Group'
2. Add > Listener > Aggregate Report

## Start Test

1. Click Star button Run Test Plan above the toolbar 

## View Test Results

1. Clcik 'View Results Tree' can view report

## Aggregate Report

1. Click 'Aggregate Report' can view report

```
Samples：發送請求筆數 (10筆)
Average：平均回應時間 (1120ms)
Median：回應時間的中間值 (830ms)
90% Line：百分之 90 的最高回應時間 (2127ms)
95% Line：百分之 95 的最高回應時間 (2127ms)
99% Line：百分之 99 的最高回應時間 (2575ms)
Min：回應時間的最小值 (322ms)
Maximum：回應時間的最大值 (2575ms)
Error %：執行的錯誤率 (0.00%)
Throughput：吞吐量 (2.7/sec)
Received KB/sec：下載速率 (718.29)
Sent KB/sec：上傳速率 (1.05)
```

## Generate Report

1. I got .jmx in GUI
example 'bobtest.jmx'

2. create folder Name 'folderName'

`sh jmeter -n -t <bobtest.jmx>.jmx -l <resultName>.jtl -e -o /<folderName>`

```
-n：以非 GUI 的方式執行 JMeter，也就是以 CLI 進行操作
-t：執行測試腳本的路徑，副檔名為 .jmx (testScript.jmx)
-l：記錄測試結果的檔案路徑與名字，副檔名為 .jtl (testResult.jtl)
-e：測試腳本執行完後產生 html 報告
-o：測試腳本 html 報告的資料夾，資料夾必須不存在 (ReportFolder)
```

> If get this error message ' An error occurred: Cannot write to '/<folderName>' as folder is not empty'
try -f 
`` sh jmeter -n -t "feature/component/Admin/SettingPage.jmx" -l resultName.jtl -f -e -o bobfolder`

> If got `An error occurred: Cannot write to '/folderName' as folder does not exist and parent folder is not writable`
please, create folder 'folderName'

After execution the commaned, can find the index.html file in folderName

# About Report Dashboard

1. Test and Report informations：
* File: saved file name
* Start Time
* End Time

2. APDEX(Application Performance Index):
* Range between 0-1，1 is the best, otherwise 0 is worst
* It shows that the server responds quickly.

3. Requests Summary:
* Requests is status code 200 OK, and fild KO display percentage

4. Statistics:
*  Summary Report and Aggrerate Report merged report

5. Errors:
* Error conditions, according to different error types, all error results are displayed.
* may be 404, 503 status code

6. Top 5 Errors by sampler：
* Top5 error message sampling

## Dashboard SideBar

### Over Time 

1. Response Times Over Time
* Response time distribution curve within script running time

2. Response Time Percentiles Over Time (successful responses) 
* Requests successfully responded to within the script running time, the response time percentile

3. Active Threads Over Time
* Distribution of active threads during script running time

4. Bytes Throughput Over Time
* The throughput during the running time of the script, in bytes

5. Latencies Over Time 
* Millisecond response delay within script running time
* Here, I use Gaussian Random Timer
* Gaussian random timer inculed Deviation (Similar to manual) , Constant Delay offset (loading time)
* Total delay time = Deviation + Constant Delay offset

6. Connect Time Over Time
* Average connection time during script running time

### Throughput
1. Hits Per Second (excluding embedded resources)
* Hits per second curve

2. Codes Per Second (excluding embedded resources)
* Status code distribution curve per second

3. Transactions Per Second
* Transactions per second curve

4. Response Time Vs Request
* The relationship between the median response time and the number of requests per second

5. Latency Vs Request
* The relationship curve between the median delay time and the number of requests per second

### Response Times
1. Response Time Percentiles
* Millisecond percentile response time curve

2. Response Time Overview 
* Response time overview histogram

3. Time Vs Threads
* Change curve of active thread and average response time

4. Response Time Distribution
* Response time distribution chart
